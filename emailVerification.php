<?php

$error = "";
if (isset($_POST['email'])) {

    $email = (string) $_POST['email'];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error = "Email is bad";
        return;
    }

    $error = validateEmail($email, $error);
}

function validateEmail($email, $error) {

    $mailparts = explode("@", $email);
    $hostname = $mailparts[1];
    $b_mx_avail = getmxrr($hostname, $mx_records, $mx_weight);
    $b_server_found = 0;


    if ($b_mx_avail) {

        $mxs = array();
        for ($i = 0; $i < count($mx_records); $i++) {
            $mxs[$mx_weight[$i]] = $mx_records[$i];
        }

        ksort($mxs, SORT_NUMERIC);
        reset($mxs);


        while (list ($mx_weight, $mx_host) = each($mxs)) {

            if ($b_server_found == 0) {

                $fp = fsockopen($mx_host, 25);

                if ($fp) {
                    send_command($fp, "HELO gmail.com");
                    send_command($fp, "MAIL FROM:<ibondariev@gmail.com>");
                    $rcpt_text = send_command($fp, "RCPT TO:<" . $email . ">");
                    if (substr($rcpt_text, 0, 3) == "250") {
                        send_command($fp, "QUIT");
                        fclose($fp);
                        return "Email is OK";
                    }

                    send_command($fp, "QUIT");
                    fclose($fp);
                }
            }
        }
    }
    return "Email is bad";
}

function send_command($fp, $out) {
    fwrite($fp, $out . "\r\n");
    return get_data($fp);
}

function get_data($fp) {
    $s = "";
    stream_set_timeout($fp, 2);
    for ($i = 0; $i < 2; $i++) {
        $s.=fgets($fp, 1024);
    }
    return $s;
}
